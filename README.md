# Node.js e-portfolio
## Presentation
I will give you an overview of what Node.js is and what it is used for.
## Requirements
You need Node.js installed as well as MongoDB which is a No-SQL Database we'll use to store data the API receives. 
Last but not least, you need a text editor of your choice e.g. Visual Studio Code, Sublime or Atom. They are nice to use because they offer a lot more features than standard text editors like Syntax highlighting and more.
Finally, we use Postman to test our API during the development process and in the end.
### Download the following:
* [Node.js](https://nodejs.org/en/download/) available for Windows, Linux and macOS
* [MongoDB Community Edition](https://www.mongodb.com/download-center?jmp=nav#community) also available for Windows, Linux and macOS
  * [Here's](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-windows/) the guide for the installation for windows, make sure to add the mongod.exe as environment variable to make things easier
* [Postman as native app](https://www.getpostman.com/apps) (available for Windows, Linux and macOS) or as [Chrome App](https://chrome.google.com/webstore/detail/postman/fhbjgbiflinjbdggehcddcbncdddomop?hl=en-US)

## Tutorial for the hands-on part: create a simple REST-API with Node.js
The tutorial can be found [here](https://gitlab.com/SecretAccount/nodejs-demo/blob/presentation/Node.js-tutorial.pdf).
