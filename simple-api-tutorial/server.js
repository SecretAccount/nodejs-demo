// server.js

// call the packages we need
var express = require('express'); // call express
var app = express(); // define our app using express
var bodyParser = require('body-parser'); //helps us to parse request better
var mongoose = require('mongoose'); // object data modeling (ODM) library
var morgan = require('morgan'); // logging tool
var port = 8080; // set our port
var database = 'mongodb://127.0.0.1:27017'; //database url

// TODO: import Cat database model and create database dir

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// use morgan to log requests to the console
app.use(morgan('dev'));

// TODO: connect to MongoDB using mongoose

// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
app.get('/', (req, res) => {
    res.json({ message: 'Hello and welcome to our Cat api at http://localhost:' + port + '/api' });
});

//TODO: add correct responses for GET, POST, PUT, DELETE 
//      to act like a RESTful API


// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);