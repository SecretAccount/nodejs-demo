// app/models/cat.js

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CatSchema = new Schema({
    name: String,
    age: String
});

module.exports = mongoose.model('Cat', CatSchema);