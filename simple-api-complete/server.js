// server.js

//import cat database model
var Cat = require('./app/models/cat');

// call the packages we need
var express = require('express'); // call express
var app = express(); // define our app using express
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var morgan = require('morgan');
var port = 8080; // set our port
var database = 'mongodb://127.0.0.1:27017'; //database url

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// use morgan to log requests to the console
app.use(morgan('dev'));

//MongoDB connection
mongoose.connect(database, function(err) {
    if (err) {
        console.log('Connection to Database not possible!');
    } else {
        console.log('Database connection established successfully!');
    }
});


// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
app.get('/', (req, res) => {
    res.json({ message: 'Hello and welcome to our Cat api at http://localhost:' + port + '/api' });
});


// get all cats (accessed at GET http://localhost:8080/api/cats)
app.get('/api/cats', (req, res) => {
    Cat.find((err, cats) => { //searches for all cats in the db
        if (err) {
            res.send(err);
        }
        res.json(cats);
    });
});


// create a cat (accessed at POST http://localhost:8080/api/cats)
app.post('/api/cats', (req, res) => {

    var cat = new Cat(); // create a new instance of the cat model
    cat.name = req.body.name; // set the cats name (comes from the request)
    cat.age = req.body.age; // set the cats name (comes from the request)

    // save the cat and check for errors
    cat.save(function(err) {
        if (err)
            res.send(err);

        res.json({ message: 'Cat created!' });
    });
});

// get a cat (accessed at GET http://localhost:8080/api/cats/:cat_id)
app.get('/api/cats/:cat_id', (req, res) => {
    //search for a cat with cat_id from url
    Cat.findById(req.params.cat_id, (err, cat) => {
        if (err) {
            res.send(err);
        }
        res.json(cat);
    });
});

// update a cat (accessed at PUT http://localhost:8080/api/cats/:cat_id)
app.put('/api/cats/:cat_id', (req, res) => {
    // use our cat model to find the cat we want
    Cat.findById(req.params.cat_id, function(err, cat) {
        if (err)
            res.send(err);
        console.log(req.body);
        if (cat) {
            cat.name = req.body.name; // update the cats name
            cat.age = req.body.age; // update the cats age
            // save the cat
            cat.save(function(err) {
                if (err)
                    res.send(err);

                res.json({ message: 'Cat updated!' });
            });
        } else {
            res.json({ message: 'Cat not found!' });
        }
    });
});

app.delete('/api/cats/:cat_id', (req, res) => {
    Cat.remove({
        _id: req.params.cat_id
    }, function(err, cat) {
        if (err)
            res.send(err);

        res.json({ message: 'Successfully deleted' });
    });
});

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);